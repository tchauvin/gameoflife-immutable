package gol;

import io.vavr.collection.Iterator;
import io.vavr.collection.Set;
import org.immutables.value.Value;

@Value.Immutable
public interface Position {
    @Value.Parameter
    int x();

    @Value.Parameter
    int y();

    default Set<Position> neighbours() {
        return Iterator
                .rangeClosed(-1, 1).flatMap(dx ->
                        Iterator.rangeClosed(-1, 1).map(dy ->
                                Position.of(x() + dx, y() + dy)
                        )
                )
                .filter(p -> !this.equals(p))
                .toSet();
    }

    static Position of(int x, int y) {
        return ImmutablePosition.of(x, y);
    }
}

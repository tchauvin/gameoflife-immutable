package gol;

public enum CellState {
    ALIVE,
    DEAD;

    public static CellState evolve(int neighbours, CellState state) {
        if (neighbours < 2 || neighbours > 3) {
            return DEAD;
        }
        if (neighbours == 3) {
            return ALIVE;
        }
        return state;
    }
}

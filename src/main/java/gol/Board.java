package gol;

import io.vavr.API;
import io.vavr.Tuple;
import io.vavr.collection.Set;
import org.immutables.value.Value;

@Value.Immutable
public interface Board {

    @Value.Parameter
    Set<Position> alivePositions();

    default Set<Position> positionsToExamine() {
        return alivePositions().union(alivePositions().flatMap(Position::neighbours));
    }

    default CellState cellState(Position position) {
        return this.alivePositions().contains(position) ? CellState.ALIVE : CellState.DEAD;
    }

    default CellState nextCellState(Position position) {
        final int aliveNeighbours = aliveNeighbours(position);
        final CellState state = cellState(position);
        return CellState.evolve(aliveNeighbours, state);
    }

    default Board nextState() {
        Set<Position> nextAlivePositions = positionsToExamine()
                .map(position -> Tuple.of(position, nextCellState(position)))
                .filter(p -> p._2 == CellState.ALIVE)
                .map(p -> p._1);
        return Board.of(nextAlivePositions);
    }

    default int aliveNeighbours(Position position) {
        return position.neighbours().count(neighbour -> cellState(neighbour) == CellState.ALIVE);
    }

    static Board of(Set<Position> alivePositions) {
        return ImmutableBoard.of(alivePositions);
    }
}

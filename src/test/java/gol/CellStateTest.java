package gol;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.EnumSource;

import static org.junit.jupiter.api.Assertions.*;

class CellStateTest {
    @ParameterizedTest
    @EnumSource(CellState.class)
    void cellWithOneOrLessAliveNeighbourShouldDieDueToUnderpopulation(final CellState state) {
        assertEquals(CellState.DEAD, CellState.evolve(0, state));
    }

    @ParameterizedTest
    @EnumSource(CellState.class)
    void cellWithTwoAliveNeighboursShouldStayTheSame(final CellState state) {
        assertEquals(state, CellState.evolve(2, state));
    }

    @ParameterizedTest
    @EnumSource(CellState.class)
    void cellWithThreeAliveNeighboursShouldBeBornDueToReproduction(final CellState cellState) {
        assertEquals(CellState.ALIVE, CellState.evolve(3, cellState));
    }

    @ParameterizedTest
    @CsvSource({
            "4, ALIVE",
            "4, DEAD",
            "5, ALIVE",
            "5, DEAD",
            "6, ALIVE",
            "6, DEAD",
            "7, ALIVE",
            "7, DEAD",
            "8, ALIVE",
            "8, DEAD"
    })
    void cellWithMoreThanThreeAliveNeighboursShouldDieDueToOverpopulation(final int neighbours, final CellState state) {
        assertEquals(CellState.DEAD, CellState.evolve(neighbours, state));
    }
}
package gol;

import io.vavr.collection.HashSet;
import io.vavr.collection.Set;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class PositionTest {
    @Test
    void neighbours() {
        Position currentPosition = Position.of(5, 6);

        Set<Position> expectedPositions = HashSet.of(
                Position.of(4, 5), Position.of(5, 5), Position.of(6, 5),
                Position.of(4, 6),                    Position.of(6, 6),
                Position.of(4, 7), Position.of(5, 7), Position.of(6, 7));

        assertThat(currentPosition.neighbours()).isEqualTo(expectedPositions);
    }
}
package gol;

import io.vavr.collection.HashSet;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class BoardTest {

    @Test
    void shouldReturnAliveNeighboursNumber() {
        Board board = Board.of(HashSet.of(
                Position.of(1, 1),
                Position.of(1, 2),
                Position.of(2, 2)
        ));

        assertAll(
                () -> assertEquals(1, board.aliveNeighbours(Position.of(1, 0))),
                () -> assertEquals(2, board.aliveNeighbours(Position.of(1, 1))),
                () -> assertEquals(3, board.aliveNeighbours(Position.of(2, 1))),
                () -> assertEquals(0, board.aliveNeighbours(Position.of(4, 1)))
        );
    }

    @Test
    void shouldReturnNextCellState() {
        Board board = Board.of(HashSet.of(
                Position.of(1, 0),
                Position.of(1, 1),
                Position.of(1, 2),
                Position.of(2, 2),
                Position.of(1, 3)
        ));

        assertEquals(CellState.ALIVE, board.nextCellState(Position.of(2, 3)));
        assertEquals(CellState.ALIVE, board.nextCellState(Position.of(0, 2)));
        assertEquals(CellState.DEAD, board.nextCellState(Position.of(1, 0)));
    }

    @Test
    void shouldReturnPositionsToExamine() {
        Board board = Board.of(HashSet.of(
                Position.of(1, 1),
                Position.of(2, 2)
        ));

        HashSet<Position> expected = HashSet.of(
                Position.of(0, 0), Position.of(1, 0), Position.of(2, 0),
                Position.of(0, 1), Position.of(1, 1), Position.of(2, 1), Position.of(3, 1),
                Position.of(0, 2), Position.of(1, 2), Position.of(2, 2), Position.of(3, 2),
                Position.of(1, 3), Position.of(2, 3), Position.of(3, 3)

        );

        assertThat(board.positionsToExamine()).isEqualTo(expected);
    }

    @Test
    void shouldReturnNextState() {
        Board board = Board.of(HashSet.of(
                Position.of(1, 0),
                Position.of(1, 1),
                Position.of(1, 2)
        ));
        Board nextBoard = Board.of(HashSet.of(
                Position.of(0, 1),
                Position.of(1, 1),
                Position.of(2, 1)
        ));

        assertThat(board.nextState()).isEqualTo(nextBoard);
    }
}